<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


/**
 * CategoryController
 *
 * @author Alexandre dos Santos Andrade
 * @version 0.0.1
 */
class CategoryController extends AbstractActionController
{
    /**
     * The default action - show the home page
     */
    public function indexAction()
    {
        // TODO Auto-generated CategoryController::indexAction() default action

        return new ViewModel();
    }
}