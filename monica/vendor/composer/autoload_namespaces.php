<?php

// autoload_namespaces.php generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'Zend\\' => $vendorDir . '/zendframework/zendframework/library/',
    'ZendTest\\' => $vendorDir . '/zendframework/zendframework/tests/',
    'TwbBundle' => $vendorDir . '/neilime/zf2-twb-bundle/src',
    'Symfony\\Component\\Yaml' => $vendorDir . '/symfony/yaml/',
    'Psr\\Log\\' => $vendorDir . '/psr/log/',
    'Monolog' => $vendorDir . '/monolog/monolog/src/',
);
